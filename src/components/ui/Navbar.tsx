import NextLink from 'next/link';
import { AppBar, Badge, Box, Button, IconButton, Input, InputAdornment, Link, Toolbar, Typography } from '@mui/material';
export const Navbar = () => {
    return (
    
      <AppBar className='bg-white'>
            <Toolbar>
                <NextLink href='/' passHref legacyBehavior>
                    <Link display='flex' alignItems='center'>
                      <img className="img-fluid" width={'100px'} src={'../../assets/img/logo/logo.png'} alt="kabeli"/>
                    </Link>  
                </NextLink>
            </Toolbar>
      </AppBar>
    )
}