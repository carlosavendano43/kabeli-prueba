import { FC, PropsWithChildren } from "react";
import Head from "next/head"

import { Navbar,Footer } from "../ui";

interface Props extends PropsWithChildren {
    title: string;
    pageDescription: string;
    imageFullUrl?: string;
}

export const MainContainer: FC<Props> = ({ children, title, pageDescription, imageFullUrl }) => {
    return (
        <>
            <Head>
                <title>
                    {title}
                </title>
                <meta name="description" content={pageDescription} />
                <meta name="description" content={pageDescription} />
                {
                    imageFullUrl && (
                        <meta name="og:image" content={pageDescription} />
                    )
                }
            </Head>
            <Navbar/>
            <main>
                {children}
            </main>
            <Footer/>
        </>
    )

}