import Image from 'next/image'
import { Inter } from 'next/font/google'
import { Grid,Box } from '@mui/material'
import { MainContainer } from '../components/layout/layout';

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <MainContainer title={'Kabeli - Home'} pageDescription={'Encuentra los mejores desarrolladores del mercado'}>
      <Grid container spacing={4}>
        <Box className="w-100">
           <img className="img-fluid" width={'100%'} src={'../../assets/img/slider/bannerKabeli.png'} alt="kabeli"/>
        </Box>
        <Box>
            <Grid container spacing={4}>
              
            </Grid>
        </Box>
      </Grid>
    </MainContainer>
  )
}
